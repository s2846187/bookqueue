package nl.utwente.di.bookQuote;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test ;
        /*** Tests the Quoter */
public class testQuoter {
        @Test
        public void testBook1() throws Exception {
                Quoter quoter = new Quoter();
                double price = quoter.getBookPrice("1");
                double price1 = quoter.getBookPrice("78");
                Assertions.assertEquals(10.0, price, 0.0, "Price of Book 1");
        }
}
