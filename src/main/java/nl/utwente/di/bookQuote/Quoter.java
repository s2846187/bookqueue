package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    double getBookPrice(String isbn){
        double price;
        HashMap<String,Double> prices = new HashMap<>();
        prices.put("1", 10.0);
        prices.put("2", 45.0);
        prices.put("3", 20.0);
        prices.put("4", 35.0);
        prices.put("5", 50.0);
        if(prices.containsKey("isbn")) {
            return prices.get("isbn");
        }
        return 0.0;
    }
}
